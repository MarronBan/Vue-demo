借助Bus事件总线 A->B(如果安装了vue-bus)  
组件A：this.$bus.emit("sendMsg",msg)  
组件B：this.$bus.on("sendMsg",(msg)=>{})  
(如果没有，自己在main里定义一个,或安装vue-bus)  
Vue.prototype.$eventBus = new Vue();  
this.$eventBus.$emit("updateMessage", this.message);  
this.$eventBus.$on('updateMessage', function(value) {  
self.updateMessage(value);  

})  
关于bus总线还有使用方法的要求（发布、订阅、移除等）；具体看indexA.vue和indexB.vue  
然后通过这种方式传值都是单向的：A->B->C->D  
如果想要B->的话得emit另一个事件