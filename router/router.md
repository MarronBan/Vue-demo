```
	#1.配置
		首先确保你的vue-cli配置了路由（初始化项目时不使用默认配置，自定义配置时选择路由）or npm install vue-router/router
	```
	```
	#2.引用进项目：在main.js里
		import Vue from 'vue'
		import VueRouter from 'vue-router'
		/import router from 'router'
		Vue.use(VueRouter/router)
		在router/index.js里
  	let routes = [{
    		path: '会在地址栏显示的路径',
    		component: () => import('项目里该组件的路径'),
    		name: '组件的唯一名称'
  		},]
  		不指定component的话在前面import就行，有两种声明方式，最好统一。
  		还有一种方式：用于定义展示首页，可以根据登录账号的权限不同，展示不同页面。
  		let routes = [{
    		path: '会在地址栏显示的路径',
    		redirect:to=>{
    			if(...) return '某组件的path';
    		}
			},]
	```
	```
	#3.功能
		##页面跳转1
		<router-link to="url">xxx</router-link>
		<router-view></router-view>
		<router-link>会被渲染成<a>标签，匹配到的页面会渲染到<router-view>上
		所有组件/页面的显示都基于App.vue
		##App.vue
		<template>
	  		<div id="app">
	   			<el-container>
	      		<router-view/>
	    		</el-container>
	  		</div>
		</template>
		如上所示，跳转后的页面替换<router-view/>显示。整个页面还是在App.vue中的；运行Vue项目，打开控制台查看Element会发现，所有的页面都包裹在<div id="app"></div>里		
	```
    	```
		##页面跳转2
		this.$router.push({ name: '组件的唯一名称', [params: { "aaa": ''}... ]})
		这种方法有历史记录，即按浏览器的返回可以回到上一页
		this.$router.replace({ name: '组件的唯一名称', [params: { "aaa": ''}... ]})
		这种方法没有历史记录，点击返回会返回到上上个页面
	```
	```
		##页面跳转3
		this.$router.back(n)
		不会刷新页面
		this.$router.go(n)
		会刷新页面
		n为正数时向下一页走，n为负数时向上一页走。此功能在无痕模式下无效
	```
	
	```
		##生命周期钩子
			beforeRouteEnter(to, from, next)
			beforeRouteUpdate(to, from, next)
			beforeRouteLeave(to, from, next)
			在无痕浏览模式下，可以使用beforeRouteLeave重定义to.name来跳转，达到返回上一页的效果。
	```
	生命周期钩子的具体使用方法见：[这里](https://www.cnblogs.com/lhl66/p/9195901.html)
	*路由还有很多功能，可以在[Vue Router](https://router.vuejs.org/zh/installation.html)官网查看*