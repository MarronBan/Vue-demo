1.Vue-cli4路径别名设置 [参考](https://www.cnblogs.com/zwnsyw/p/12316622.html)

2.使用swiper组件报错，找到报错的行，把e.preventDefault()注释掉就行了

3.在css里引用其他css要加上~

4.引入jq

​	1.npm install jquery

​	2.在vue.config.js中加入

```javascript
const webpack = require('webpack')

module.exports = {
    chainWebpack: config => {
        config.plugin('provide').use(webpack.ProvidePlugin, [{
            $: 'jquery',
            jquery: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }])
    }
}
```



​	3.需要使用的页面里 import $ from 'jquery'

5.使用ajax发送json数据，如果没有指定headers，服务器端接收到会多一个冒号；[解决方法](https://blog.csdn.net/weixin_43043994/article/details/101058969)

6.移动端点击input框，样式错位。这可能是使用了定位、框架冲突；[解决方法](https://www.cnblogs.com/cm1236/p/5510004.html)

7.项目打包后，找不到css，报net::ERR_ABORTED 404 (Not Found)；[解决方法](https://blog.csdn.net/qq_18674153/article/details/102537576)

8.阻止用户放大。一般在meta里设置；但是但是但是但是ios不行，要这么写

```javascript
window.onload = function() {
          // 阻止双击放大
          var lastTouchEnd = 0;
          document.addEventListener('touchstart', function(event) {
              if (event.touches.length > 1) {
                  event.preventDefault();
              }
          });
          document.addEventListener('touchend', function(event) {
              var now = (new Date()).getTime();
              if (now - lastTouchEnd <= 300) {
                  event.preventDefault();
              }
              lastTouchEnd = now;
          }, false);

          // 阻止双指放大
          document.addEventListener('gesturestart', function(event) {
              event.preventDefault();
          });
      }
```

​	这样写了还没完，有滚轴有动画时双指放大还是能放大。

​	试试

```html
body,
    html {
      overflow-x: hidden;
    }
```

​	如果要禁用下拉，[参考](https://www.cnblogs.com/chefweb/p/9687960.html)

```html
body,html{

　　position:fixed;

　　top:0;

　　left:0;

　　width:100%;

　　height:100%;

}
```

9.在写了footer的页面，会发现footer上面的div会有间隔。这时候找包裹上面div的最外层的div，设置padding-bottom!

设置前:

![image-20200715145803044](D:\新建文件夹\新建文件夹\assets\image-20200715145803044.png)

设置后：

![image-20200715145847799](D:\新建文件夹\新建文件夹\assets\image-20200715145847799.png)

代码：

![image-20200715145943098](D:\新建文件夹\新建文件夹\assets\image-20200715145943098.png)